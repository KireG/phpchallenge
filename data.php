<?php
//--Checking POST      
if($_SERVER["REQUEST_METHOD"] == "POST"){

    if (empty($_POST['rating']) || empty($_POST['minimum']) || empty($_POST['date']) || empty($_POST['prioritize'])){
        header('Location: view.php?error');
        die();
    } else {
        $rating=$_POST['rating'];
        $minimum=$_POST['minimum'];
        $date=$_POST['date'];
        $prioritize= $_POST['prioritize'];
    }
//--Conectiont to json file  and json_decode  
    $jsondata= file_get_contents("reviews.json");
    $datadecoded = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $jsondata), true);
//--list is final array    
    $list=[];
//--list1 is array with  data with text    
    $list1=[];
//--list2 is array with  data without text    
    $list2=[];
//--Filling list1 and list2 array  with data that have minimum rate(minimum filter)
    foreach($datadecoded as $data){
      if($data['rating']>=$minimum) {
        if (strlen($data['reviewText']) > 0){
            array_push($list1,$data);    
        } else if (strlen($data['reviewText']) == 0){
            array_push($list2,$data);
        }
      }     
    }
//--Multiple filter for list1 rate filter and date filter.Rate filter has priority    
    $rat = [];
    $dat = [];
    foreach ($list1 as $key => $row) {
        $rat[$key]  = $row['rating'];
        $dat[$key] = $row['reviewCreatedOnDate'];
    }
    if($rating == 1 && $date == 1){
        array_multisort($rat, SORT_DESC, $dat, SORT_ASC, $list1);
    }else if ($rating == 1 && $date == 2){
        array_multisort($rat, SORT_DESC, $dat, SORT_DESC, $list1);
    }else if ($rating == 2 && $date == 2){
        array_multisort($rat, SORT_ASC, $dat, SORT_DESC, $list1);
    }else if($rating == 2 && $date == 1){
        array_multisort($rat, SORT_ASC, $dat, SORT_ASC, $list1);
        }
//--Multiple filter for list2 rate filter and date filter.Rate filter has priority   
    $rat2 = [];
    $dat2 = [];
    foreach ($list2 as $key => $row) {
        $rat2[$key]  = $row['rating'];
        $dat2[$key] = $row['reviewCreatedOnDate'];
    }
    if($rating == 1 && $date == 1){
        array_multisort($rat2, SORT_DESC, $dat2, SORT_ASC, $list2);
    }else if ($rating == 1 && $date == 2){
        array_multisort($rat2, SORT_DESC, $dat2, SORT_DESC, $list2);
    }else if ($rating == 2 && $date == 2){
        array_multisort($rat2, SORT_ASC, $dat2, SORT_DESC, $list2);
    }else if($rating == 2 && $date == 1){
        array_multisort($rat2, SORT_ASC, $dat2, SORT_ASC, $list2);
    } 
//--Prioritize by text filter         
    if ($prioritize == 1){
        array_push($list,$list1,$list2);
    } else if ($prioritize == 2){
        array_push($list,$list2,$list1);
    }
} else{
    header('Location: view.php');
};   
?>
<html>
    <head>
    <link href="styles.css" rel="stylesheet">
    </head>
    <body>
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Rating</th>
                        <th>Text</th>
                        <th></th>
                        <th style="width:200px">Date</th>
                    </tr>
                </thead>
            <tbody>
            <?php
            if($_POST['rating']){ 
               foreach ($list as $li){
                 foreach ($li as $l){?>
                    <tr>
                        <td style="text-align:center;"><?php echo ($l['rating'])?></td> 
                        <td><?php echo ($l['reviewText'])?><td>
                        <td><?php echo ($l['reviewCreatedOnDate'])?></td>
                    </tr>    
             <?php   
                }
            }
        } 
           ?>
            </tbody>
           </table>
           <p style="text-align:center;"><a href="view.php">Back</a></p>
        </div>
    </body>
</html>
        
         
        
            
       
       
