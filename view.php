<!DOCTYPE html>

<html>
<head>
<link href="styles.css" rel="stylesheet">
</head>
<body>
    <div class="main">
    <?php
        if (isset($_GET['error'])){
            echo "<p class='alert alert-success' style='color:red;text-align:center'>All fields are required</p>";
        };
    ?>
    <h2>Filter reviews</h2>
    <form action="data.php " method="POST">
    <div class="selectsDiv">
        <p>Order by rating:</p>
        <select name="rating" id="">
            <option value=""></option>
            <option value="1">Highest first</option>
            <option value="2">Lowest first</option>
        </select>
    </div>
    <div class="selectsDiv">
        <p>Minimum rating:</p>
        <select name="minimum" id="">
            <option value=""></option>
            <option value="5">5</option>
            <option value="4">4</option>
            <option value="3">3</option>
            <option value="2">2</option>
            <option value="1">1</option>
        </select>
    </div>
    <div class="selectsDiv">
        <p>Order by date:</p>
        <select name="date" id="">
            <option value=""></option>
            <option value="1">Oldest first</option>
            <option value="2">Newest first</option>
        </select>
    </div>
    <div class="selectsDiv">
        <p>Prioritize by text:</p>
        <select name="prioritize" id="">
            <option value=""></option>
            <option value="1">Yes</option>
            <option value="2">No</option>
        </select>
    </div>
    <button>Filter</button>
    </form>
    </div> 
</body>
</html>